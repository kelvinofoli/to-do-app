import { State, Action, StateContext , Selector} from '@ngxs/store';
import { AddTodo, RemoveTodo } from './../actions/todo.actions';
import {Todo} from './../models/todo.model';

export class TodoStateModel {
    todos: Todo[];
}

@State<TodoStateModel>({
    name: 'todos',
    defaults: {
      todos: []
    }
})
export class TodoState {

  @Selector()
    static getTodos(state: TodoStateModel) {
        return state.todos;
  }

  @Action(AddTodo)
  add({ getState , patchState }: StateContext<TodoStateModel>, { payload }: AddTodo) {
      const state = getState();
      patchState({
          todos: [...state.todos, payload]
      });
  }

  @Action(RemoveTodo)
  remove({ getState , patchState }: StateContext<TodoStateModel>, { payload }: RemoveTodo) {
      patchState({
            todos: getState().todos.filter(
              a => {
                return(a.index !== payload.index);
              })
      });
  }
}
