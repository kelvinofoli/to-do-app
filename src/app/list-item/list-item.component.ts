import { Component, OnInit, Input  , Output , EventEmitter} from '@angular/core';
import { Store } from '@ngxs/store';


@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input()item: {index: number, text: string};
  // tslint:disable-next-line: no-output-rename
  @Output('tdRemove') removeTodo = new EventEmitter<{index: number}>();

  constructor(public store: Store) { }

  ngOnInit() {
  }

  OnRemoveTodo() {
    this.removeTodo.emit({
      index : this.item.index
    });
  }


}
