export interface Todo {
  index: number;
  text: string;
}
