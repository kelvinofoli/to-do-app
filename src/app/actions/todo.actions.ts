import {Store} from '@ngxs/store';
import {Todo} from './../models/todo.model';

export class AddTodo {
  static readonly type = '[TODOS] Add';
  constructor(public payload: Todo) {}
}

export class RemoveTodo {
  static readonly type = '[TODOS] Remove';
  constructor(public payload: any) {}
}
