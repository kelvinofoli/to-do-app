import { TodoState } from './../state/todo.state';
import { Todo } from './../models/todo.model';
import { AddTodo , RemoveTodo } from './../actions/todo.actions';
import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  index = 0;
  // todos$: Observable<Todo>;
  @Select(TodoState.getTodos) todos$: Observable<Todo>;

  constructor(private store: Store) {
    // this.todos$ = this.store.select(state => state.todos.todos);
  }

  ngOnInit() {
  }

  addTodo(todo) {
    this.store.dispatch(new AddTodo({index: this.index, text: todo}));
    this.index++;
  }

  removeTodo(index) {
    console.log(index);
    this.store.dispatch(new RemoveTodo(index));
  }
}
